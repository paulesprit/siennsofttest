﻿using FluentValidation;
using SIENN.Interfaces.Unit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SIENN.WebApi.Validators
{
    public class UnitValidator : AbstractValidator<UnitDto>
    {
        public UnitValidator()
        {
            RuleFor(m => m.Code).NotEmpty().MaximumLength(5);
            RuleFor(m => m.Description).NotEmpty();
        }
    }
}
