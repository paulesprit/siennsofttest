﻿using FluentValidation;
using SIENN.Interfaces.Category;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SIENN.WebApi.Validators
{
    public class CategoryValidator : AbstractValidator<CategoryDto>
    {
        public CategoryValidator()
        {
            RuleFor(m => m.Code).NotEmpty().MinimumLength(2);
            RuleFor(m => m.Description).NotEmpty();
        }
    }
}
