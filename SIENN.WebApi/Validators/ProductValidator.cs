﻿using FluentValidation;
using SIENN.Interfaces.Product;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SIENN.WebApi.Validators
{
    public class ProductValidator : AbstractValidator<ProductDto>
    {
        public ProductValidator()
        {
            RuleFor(m => m.Code).NotEmpty().MinimumLength(2);
            RuleFor(m => m.Description).NotEmpty();
            RuleFor(m => m.Name).NotEmpty();
        }
    }
}
