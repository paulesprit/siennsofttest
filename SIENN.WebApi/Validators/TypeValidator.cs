﻿using FluentValidation;
using SIENN.Interfaces.Type;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SIENN.WebApi.Validators
{
    public class TypeValidator : AbstractValidator<TypeDto>
    {
        public TypeValidator()
        {
            RuleFor(m => m.Code).NotEmpty().MinimumLength(2);
            RuleFor(m => m.Description).NotEmpty();
        }
    }
}
