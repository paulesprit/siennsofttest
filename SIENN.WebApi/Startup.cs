﻿using AutoMapper;
using FluentValidation.AspNetCore;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using SIENN.DbAccess;
using SIENN.DbAccess.Repositories;
using SIENN.Interfaces;
using SIENN.Services;
using SIENN.WebApi.Filters;
using Swashbuckle.AspNetCore.Swagger;

namespace SIENN.WebApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            string connection = Configuration.GetConnectionString("DefaultConnection");

            services.AddDbContext<SIENNContext>(options =>
                options.UseSqlServer(connection));

            services.AddTransient<IUnitOfWork, UnitOfWork>();

            services.AddScoped<IGenericRepository<UnitEntity>, UnitRepository>();
            services.AddTransient<UnitService, UnitService>();

            services.AddScoped<IGenericRepository<CategoryEntity>, CategoryRepository>();
            services.AddTransient<CategoryService, CategoryService>();

            services.AddScoped<IGenericRepository<TypeEntity>, TypeRepository>();
            services.AddTransient<TypeService, TypeService>();

            services.AddScoped<IGenericRepository<ProductEntity>, ProductRepository>();
            services.AddTransient<ProductService, ProductService>();

            services.AddAutoMapper();

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info
                {
                    Version = "v1",
                    Title = "SIENN Recruitment API"
                });
            });

            services.AddMvc(opt =>
            {
                opt.Filters.Add(typeof(ValidatorActionFilter));
            }).AddFluentValidation(fvc => fvc.RegisterValidatorsFromAssemblyContaining<Startup>()); ;
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                using (var serviceScope = app.ApplicationServices.GetRequiredService<IServiceScopeFactory>().CreateScope())
                {

                    var context = serviceScope.ServiceProvider.GetRequiredService<SIENNContext>();
                    context.Database.Migrate();
                    serviceScope.ServiceProvider.GetService<SIENNContext>().EnsureSeeded();
                }
            }

            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "SIENN Recruitment API v1");
            });

            app.UseMvc();
        }
    }
}
