﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SIENN.Services;
using SIENN.DbAccess.Repositories;
using SIENN.Interfaces.Unit;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace SIENN.WebApi.Controllers
{
    [Route("api/[controller]")]
    public class UnitController : Controller
    {
        UnitService service;

        public UnitController(UnitService service)
        {
            this.service = service;
        }

        // GET: api/unit
        [HttpGet]
        public IEnumerable<UnitDto> Get()
        {            
            return service.List();
        }

        // GET api/unit/5
        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            var unitDto = service.Get(id);
            return new ObjectResult(unitDto);
        }

        // POST api/unit
        [HttpPost]
        public IActionResult Post([FromBody]UnitDto unit)
        {
            var unitDto = service.Add(unit);
            return Ok(unitDto);
        }

        // PUT api/unit/5
        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody]UnitDto unit)
        {
            var unitDto = service.Update(unit);
            return Ok(unitDto);
        }

        // DELETE api/umit/5
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            service.Remove(id);
            return Ok();
        }
    }
}
