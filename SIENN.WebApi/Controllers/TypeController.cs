﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SIENN.Services;
using SIENN.Interfaces.Type;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace SIENN.WebApi.Controllers
{
    [Route("api/[controller]")]
    public class TypeController : Controller
    {
        TypeService service;

        public TypeController(TypeService service)
        {
            this.service = service;
        }

        // GET: api/Type
        [HttpGet]
        public IEnumerable<TypeDto> Get()
        {
            return service.List();
        }

        // GET api/Type/5
        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            var typeDto = service.Get(id);
            return new ObjectResult(typeDto);
        }

        // POST api/Type
        [HttpPost]
        public IActionResult Post([FromBody]TypeDto type)
        {
            var typeDto = service.Add(type);
            return Ok(typeDto);
        }

        // PUT api/Type/5
        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody]TypeDto type)
        {
            var typeDto = service.Update(type);
            return Ok(typeDto);

        }

        // DELETE api/Type/5
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            service.Remove(id);
            return Ok();
        }
    }
}
