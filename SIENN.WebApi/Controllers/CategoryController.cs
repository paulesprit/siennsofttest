﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SIENN.Services;
using SIENN.Interfaces.Category;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace SIENN.WebApi.Controllers
{
    [Route("api/[controller]")]
    public class CategoryController : Controller
    {
        CategoryService service;

        public CategoryController(CategoryService service)
        {
            this.service = service;
        }

        // GET: api/Category
        [HttpGet]
        public IEnumerable<CategoryDto> Get()
        {
            return service.List();
        }

        // GET api/Category/5
        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            var categoryDto = service.Get(id);
            return new ObjectResult(categoryDto);
        }

        // POST api/Category
        [HttpPost]
        public IActionResult Post([FromBody]CategoryDto category)
        {
            var categoryDto = service.Add(category);
            return Ok(categoryDto);
        }

        // PUT api/Category/5
        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody]CategoryDto category)
        {
            var categoryDto = service.Update(category);
            return Ok(categoryDto);
        }

        // DELETE api/Category/5
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            service.Remove(id);
            return Ok();
        }
    }
}
