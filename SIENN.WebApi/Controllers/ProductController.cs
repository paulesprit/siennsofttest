﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SIENN.Services;
using SIENN.Interfaces.Product;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace SIENN.WebApi.Controllers
{
    [Route("api/[controller]")]
    public class ProductController : Controller
    {
        ProductService service;

        public ProductController(ProductService service)
        {
            this.service = service;
        }

        // GET: api/Product
        [HttpGet]
        public async Task<IEnumerable<ProductDto>> GetAsync()
        {
            return await service.ListAsync();
        }

        // GET: api/Product/ListAvailable
        [HttpGet]
        [Route("ListAvailable")]
        public async Task<IEnumerable<ProductDto>> ListAvailableAsync(int start, int count)
        {
            return await service.ListAvailableAsync(start, count);
        }

        // GET: api/Product/ListFilter
        [HttpGet]
        [Route("ListFilter")]
        public async Task<IEnumerable<ProductDto>> ListFilterAsync(int unitId, int typeId, int categoryId)
        {
            return  await service.ListFilterAsync(unitId, typeId, categoryId);
        }

        // GET: api/Product/ProductInfo
        [HttpGet]
        [Route("ProductInfo")]
        public async Task<IActionResult> ProductInfoAsync(int productId)
        {
            var productInfoDto = await service.GetProductInfoAsync(productId);
            return new ObjectResult(productInfoDto);
        }

        // GET api/Product/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetAsync(int id)
        {
            var productDto = await service.GetAsync(id);
            return new ObjectResult(productDto);
        }

        // POST api/Product
        [HttpPost]
        public async Task<IActionResult> PostAsync([FromBody]ProductDto product)
        {
            var productDto = await service.AddAsync(product);
            return Ok(productDto);
        }

        // PUT api/Product/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutAsync(int id, [FromBody]ProductDto product)
        {
            var productDto = await service.UpdateAsync(product);
            return Ok(productDto);
        }

        // DELETE api/Product/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteAsync(int id)
        {
            await service.RemoveAsync(id);
            return Ok();
        }
    }
}
