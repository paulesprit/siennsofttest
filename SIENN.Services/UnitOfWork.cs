﻿using Microsoft.EntityFrameworkCore;
using SIENN.DbAccess;
using SIENN.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace SIENN.Services
{
    public class UnitOfWork : IUnitOfWork
    {
        SIENNContext context;

        public UnitOfWork(SIENNContext context)
        {
            this.context = context;
        }

        public void Save()
        {
            context.SaveChanges();
        }
    }
}
