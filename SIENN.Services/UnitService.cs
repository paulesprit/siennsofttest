﻿using AutoMapper;
using SIENN.DbAccess;
using SIENN.DbAccess.Repositories;
using SIENN.Interfaces;
using SIENN.Interfaces.Unit;
using System;
using System.Collections.Generic;
using System.Text;

namespace SIENN.Services
{
    public class UnitService : IUnitService
    {

        IGenericRepository<UnitEntity> unitRepository;
        IUnitOfWork unitOfWork;

        public UnitService(IGenericRepository<UnitEntity> unitRepository, IUnitOfWork unitOfWork)
        {
            this.unitRepository = unitRepository;
            this.unitOfWork = unitOfWork;
        }

        public UnitDto Add(UnitDto unitDto)
        {
            Mapper.Initialize(cfg => cfg.CreateMap<UnitDto, UnitEntity>());
            var unitEntity = Mapper.Map<UnitDto, UnitEntity>(unitDto);

            unitRepository.Add(unitEntity);
            unitOfWork.Save();

            Mapper.Initialize(cfg => cfg.CreateMap<UnitEntity, UnitDto>());
            Mapper.Map<UnitEntity, UnitDto>(unitEntity, unitDto);

            return unitDto;

        }

        public UnitDto Get(int unitId)
        {
            var unitEntity = unitRepository.Get(unitId);

            Mapper.Initialize(cfg => cfg.CreateMap<UnitEntity, UnitDto>());
            var unitDto = Mapper.Map<UnitEntity,UnitDto>(unitEntity);

            return unitDto;
        }

        public IList<UnitDto> List()
        {
            var unitsEntity = unitRepository.GetAll();

            Mapper.Initialize(cfg => cfg.CreateMap<UnitEntity, UnitDto>());
            var unitsDto = Mapper.Map<IEnumerable<UnitEntity>, List<UnitDto>>(unitsEntity);

            return unitsDto;
        }

        public void Remove(int unitId)
        {
            var unitEntity = unitRepository.Get(unitId);
            unitRepository.Remove(unitEntity);
            unitOfWork.Save();
        }

        public UnitDto Update(UnitDto unitDto)
        {
            Mapper.Initialize(cfg => cfg.CreateMap<UnitDto, UnitEntity>());
            var unitEntity = Mapper.Map<UnitDto, UnitEntity>(unitDto);

            unitRepository.Update(unitEntity);
            unitOfWork.Save();

            Mapper.Initialize(cfg => cfg.CreateMap<UnitEntity, UnitDto>());
            Mapper.Map(unitEntity, unitDto);

            return unitDto;
        }
    }
}
