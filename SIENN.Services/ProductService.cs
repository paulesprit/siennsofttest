﻿using AutoMapper;
using SIENN.DbAccess;
using SIENN.DbAccess.Repositories;
using SIENN.Interfaces.Product;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Linq;
using System.Text;
using SIENN.Interfaces;
using System.Threading.Tasks;

namespace SIENN.Services
{
    public class ProductService : IProductService
    {
        IGenericRepository<ProductEntity> productRepository;
        IUnitOfWork unitOfWork;

        public ProductService(IGenericRepository<ProductEntity> productRepository, IUnitOfWork unitOfWork)
        {
            this.productRepository = productRepository;
            this.unitOfWork = unitOfWork;
        }

        public Task<ProductDto> AddAsync(ProductDto productDto)
        {
            return Task.Run(() =>
            {
                InitMapProductDtoEntity();
                var productEntity = Mapper.Map<ProductDto, ProductEntity>(productDto);

                productRepository.Add(productEntity);
                unitOfWork.Save();

                InitMapProductEntityToDto();
                Mapper.Map<ProductEntity, ProductDto>(productEntity, productDto);

                return productDto;
            });
        }

        public Task<ProductDto> GetAsync(int productId)
        {
            return Task.Run(() =>
            {
                var productEntity = productRepository.Get(productId);

                InitMapProductEntityToDto();
                var productDto = Mapper.Map<ProductEntity, ProductDto>(productEntity);

                return productDto;
            });
        }

        public Task<List<ProductDto>> ListFilterAsync(int unitId, int typeId, int categoryId)
        {
            return Task.Run(() =>
            {
                Expression<Func<ProductEntity, bool>> expression = (p) => true;
                if (unitId > 0)
                {
                    expression = expression.AndAlso(product => product.UnitId == unitId);
                }
                if (typeId > 0)
                {
                    expression = expression.AndAlso(product => product.TypeId == typeId);
                }
                if (categoryId > 0)
                {
                    expression = expression.AndAlso(product => product.Categories.Any(c => c.Category.Id == categoryId));
                }

                var productsEntity = productRepository.Find(expression);

                InitMapProductEntityToDto();

                var productsDto = Mapper.Map<IEnumerable<ProductEntity>, List<ProductDto>>(productsEntity);

                return productsDto;
            });
        }

        public Task<List<ProductDto>> ListAsync()
        {
            return Task.Run(() =>
            {
                var productsEntity = productRepository.GetAll();

                InitMapProductEntityToDto();

                var productsDto = Mapper.Map<IEnumerable<ProductEntity>, List<ProductDto>>(productsEntity);

                return productsDto;
            });
        }

        public Task RemoveAsync(int productId)
        {
            return Task.Run(() =>
            {
                var productEntity = productRepository.Get(productId);
                productRepository.Remove(productEntity);
                unitOfWork.Save();
            });
        }

        public Task<ProductDto> UpdateAsync(ProductDto productDto)
        {
            return Task.Run(() =>
            {
                InitMapProductDtoEntity();
                var productEntity = Mapper.Map<ProductDto, ProductEntity>(productDto);

                productRepository.Update(productEntity);
                unitOfWork.Save();

                InitMapProductEntityToDto();
                Mapper.Map(productEntity, productDto);

                return productDto;
            });
        }

        public Task<List<ProductDto>> ListAvailableAsync(int start, int count)
        {
            return Task.Run(() =>
            {
                Expression<Func<ProductEntity, bool>> filterExp = product => product.IsAvailable == true;

                var productsEntity = productRepository.GetRange(start, count, filterExp);

                InitMapProductEntityToDto();

                var productsDto = Mapper.Map<IEnumerable<ProductEntity>, List<ProductDto>>(productsEntity);

                return productsDto;
            });
        }

        private static void InitMapProductEntityToDto()
        {
            Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<ProductEntity, ProductDto>();

                cfg.CreateMap<ProductCategoryEntity, ProductCategoryDto>()
                .ForMember(s => s.CategoryId, c => c.MapFrom(m => m.CategoryId))
                .ForMember(s => s.ProductId, c => c.MapFrom(m => m.ProductId));

            });
        }

        private static void InitMapProductDtoEntity()
        {
            Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<ProductDto, ProductEntity>();

                cfg.CreateMap<ProductCategoryDto, ProductCategoryEntity>()
                .ForMember(s => s.CategoryId, c => c.MapFrom(m => m.CategoryId))
                .ForMember(s => s.ProductId, c => c.MapFrom(m => m.ProductId));

            });
        }

        public Task<ProductInfoDto> GetProductInfoAsync(int productId)
        {
            return Task.Run(() =>
            {
                var productEntity = productRepository.Get(productId);

                var productInfoDto = GetProductInfo(productEntity);

                return productInfoDto;
            });
        }

        private ProductInfoDto GetProductInfo(ProductEntity product)
        {
            var productInfoDto = new ProductInfoDto();

            if (product != null)
            {
                productInfoDto.ProductDescription = $"({product.Code}) {product.Description}";
                productInfoDto.Price = $"{product.Price.ToString("0.##")} zl";
                productInfoDto.IsAvailable = product.IsAvailable ? "Available" : "Unavailable";
                productInfoDto.DeliveryDate = product.DeliveryDate?.ToString("dd.MM.yyyy");
                productInfoDto.CategoriesCount = product.Categories != null ? product.Categories.Count : 0;
                productInfoDto.Type = product.Type != null ?
                    $"({product.Type.Code}) {product.Type.Description}"
                    : string.Empty;
                productInfoDto.Unit = product.Unit != null ?
                    $"({product.Unit.Code}) {product.Unit.Description}"
                    : string.Empty;
            }

            return productInfoDto;
        }
    }
}
