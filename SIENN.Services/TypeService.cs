﻿using AutoMapper;
using SIENN.DbAccess;
using SIENN.DbAccess.Repositories;
using SIENN.Interfaces;
using SIENN.Interfaces.Type;
using System;
using System.Collections.Generic;
using System.Text;

namespace SIENN.Services
{
    public class TypeService : ITypeService
    {

        IGenericRepository<TypeEntity> typeRepository;
        IUnitOfWork unitOfWork;

        public TypeService(IGenericRepository<TypeEntity> typeRepository, IUnitOfWork unitOfWork)
        {
            this.typeRepository = typeRepository;
            this.unitOfWork = unitOfWork;
        }

        public TypeDto Add(TypeDto typeDto)
        {
            Mapper.Initialize(cfg => cfg.CreateMap<TypeDto, TypeEntity>());
            var typeEntity = Mapper.Map<TypeDto, TypeEntity>(typeDto);

            typeRepository.Add(typeEntity);
            unitOfWork.Save();

            Mapper.Initialize(cfg => cfg.CreateMap<TypeEntity, TypeDto>());
            Mapper.Map<TypeEntity, TypeDto>(typeEntity, typeDto);

            return typeDto;

        }

        public TypeDto Get(int typeId)
        {
            var typeEntity = typeRepository.Get(typeId);

            Mapper.Initialize(cfg => cfg.CreateMap<TypeEntity, TypeDto>());
            var typeDto = Mapper.Map<TypeEntity, TypeDto>(typeEntity);

            return typeDto;
        }

        public IList<TypeDto> List()
        {
            var typesEntity = typeRepository.GetAll();

            Mapper.Initialize(cfg => cfg.CreateMap<TypeEntity, TypeDto>());
            var typesDto = Mapper.Map<IEnumerable<TypeEntity>, List<TypeDto>>(typesEntity);

            return typesDto;
        }

        public void Remove(int typeId)
        {
            var typeEntity = typeRepository.Get(typeId);
            typeRepository.Remove(typeEntity);
            unitOfWork.Save();
        }

        public TypeDto Update(TypeDto typeDto)
        {
            Mapper.Initialize(cfg => cfg.CreateMap<TypeDto, TypeEntity>());
            var typeEntity = Mapper.Map<TypeDto, TypeEntity>(typeDto);

            typeRepository.Update(typeEntity);
            unitOfWork.Save();

            Mapper.Initialize(cfg => cfg.CreateMap<TypeEntity, TypeDto>());
            Mapper.Map(typeEntity, typeDto);

            return typeDto;
        }
    }
}
