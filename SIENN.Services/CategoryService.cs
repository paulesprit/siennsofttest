﻿using AutoMapper;
using SIENN.DbAccess;
using SIENN.DbAccess.Repositories;
using SIENN.Interfaces;
using SIENN.Interfaces.Category;
using System;
using System.Collections.Generic;
using System.Text;

namespace SIENN.Services
{
    public class CategoryService : ICategoryService
    {
        IGenericRepository<CategoryEntity> categoryRepository;
        IUnitOfWork unitOfWork;

        public CategoryService(IGenericRepository<CategoryEntity> categoryRepository, IUnitOfWork unitOfWork)
        {
            this.categoryRepository = categoryRepository;
            this.unitOfWork = unitOfWork;
        }

        public CategoryDto Add(CategoryDto categoryDto)
        {
            Mapper.Initialize(cfg => cfg.CreateMap<CategoryDto, CategoryEntity>());
            var categoryEntity = Mapper.Map<CategoryDto, CategoryEntity>(categoryDto);

            categoryRepository.Add(categoryEntity);
            unitOfWork.Save();

            Mapper.Initialize(cfg => cfg.CreateMap<CategoryEntity, CategoryDto>());
            Mapper.Map<CategoryEntity, CategoryDto>(categoryEntity, categoryDto);

            return categoryDto;                                                    
        }

        public CategoryDto Get(int categoryId)
        {
            var categoryEntity = categoryRepository.Get(categoryId);

            Mapper.Initialize(cfg => cfg.CreateMap<CategoryEntity, CategoryDto>());
            var categoryDto = Mapper.Map<CategoryEntity, CategoryDto>(categoryEntity);

            return categoryDto;
        }

        public IList<CategoryDto> List()
        {
            var categoriesEntity = categoryRepository.GetAll();

            Mapper.Initialize(cfg => cfg.CreateMap<CategoryEntity, CategoryDto>());
            var categoriesDto = Mapper.Map<IEnumerable<CategoryEntity>, List<CategoryDto>>(categoriesEntity);

            return categoriesDto;
        }

        public void Remove(int categoryId)
        {
            var categoryEntity = categoryRepository.Get(categoryId);
            categoryRepository.Remove(categoryEntity);
            unitOfWork.Save();
        }

        public CategoryDto Update(CategoryDto categoryDto)
        {
            Mapper.Initialize(cfg => cfg.CreateMap<CategoryDto, CategoryEntity>());
            var categoryEntity = Mapper.Map<CategoryDto, CategoryEntity>(categoryDto);

            categoryRepository.Update(categoryEntity);
            unitOfWork.Save();

            Mapper.Initialize(cfg => cfg.CreateMap<CategoryEntity, CategoryDto>());
            Mapper.Map(categoryEntity, categoryDto);

            return categoryDto;
        }
    }
}
