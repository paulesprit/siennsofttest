﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace SIENN.DbAccess
{
    public class CategoryEntity
    {
        [Key]
        public int Id { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public ICollection<ProductCategoryEntity> Products { get; set; }
    }
}
