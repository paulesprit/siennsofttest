﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace SIENN.DbAccess
{
    public class ProductCategoryEntity
    {
        public int ProductId { get; set; }
        [ForeignKey("ProductId")]
        public ProductEntity Product { get; set; }

        public int CategoryId { get; set; }
        [ForeignKey("CategoryId")]
        public CategoryEntity Category { get; set; }
    }
}
