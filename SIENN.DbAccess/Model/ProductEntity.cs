﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace SIENN.DbAccess
{
    public class ProductEntity
    {
        [Key]
        public int Id { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        [Column(TypeName = "Money")]
        public decimal Price { get; set; }
        public bool IsAvailable { get; set; }
        public DateTime? DeliveryDate { get; set; }
        public int TypeId { get; set; }
        public virtual TypeEntity Type { get; set; }
        public int UnitId { get; set; }
        public virtual UnitEntity Unit { get; set; }
        public ICollection<ProductCategoryEntity> Categories { get; set; }
        public string Name { get; set; }

    }
}
