﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace SIENN.DbAccess
{
    public class SIENNContext : DbContext
    {

        public SIENNContext(DbContextOptions<SIENNContext> options)
            : base(options)
        {
        }

        public virtual DbSet<ProductEntity> Products { get; set; }
        public virtual DbSet<CategoryEntity> Categories { get; set; }
        public virtual DbSet<TypeEntity> Types { get; set; }
        public virtual DbSet<UnitEntity> Units { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ProductEntity>(b =>
            {
                b.Property(e => e.Id).ValueGeneratedOnAdd();
            });

            modelBuilder.Entity<CategoryEntity>(b =>
            {
                b.Property(e => e.Id).ValueGeneratedOnAdd();
            });

            modelBuilder.Entity<ProductCategoryEntity>()
            .HasKey(t => new { t.ProductId, t.CategoryId });

            modelBuilder.Entity<TypeEntity>(b =>
            {
                b.Property(e => e.Id).ValueGeneratedOnAdd();
            });

            modelBuilder.Entity<UnitEntity>(b =>
            {
                b.Property(e => e.Id).ValueGeneratedOnAdd();
            });
        }
    }

    public static class ContextExtention
    {
        public static void EnsureSeeded(this SIENNContext context)
        {

            if (!context.Types.Any())
            {
                TypeEntity foodType = new TypeEntity { Code = "FD", Description = "Food" };
                TypeEntity drinkType = new TypeEntity { Code = "WT", Description = "Water" };
                context.Types.Add(foodType);
                context.Types.Add(drinkType);
                context.SaveChanges();
            }

            if (!context.Units.Any())
            {
                UnitEntity meters = new UnitEntity { Code = "m", Description = "Meters" };
                UnitEntity kilos = new UnitEntity { Code = "kg", Description = "Kilos" };

                context.Units.Add(meters);
                context.Units.Add(kilos);
                context.SaveChanges();

            }

            if (!context.Categories.Any())
            {
                CategoryEntity beerCategory = new CategoryEntity { Code = "BR", Description = "Beers" };
                CategoryEntity wineCategory = new CategoryEntity { Code = "WN", Description = "Wines" };
                CategoryEntity alcoholCategory = new CategoryEntity { Code = "ALC", Description = "Alcohol" };
                CategoryEntity waterCategory = new CategoryEntity { Code = "DRN", Description = "Water" };

                context.Categories.Add(beerCategory);
                context.Categories.Add(wineCategory);
                context.Categories.Add(alcoholCategory);
                context.Categories.Add(waterCategory);
                context.SaveChanges();
            }

            if (!context.Products.Any())
            {
                ProductEntity product1 = new ProductEntity
                {
                    Code = "PR1",
                    Description = "Prod1",
                    IsAvailable = true,
                    Price = 12,
                    DeliveryDate = DateTime.Now,
                    Name = "PRD1",
                    TypeId = 1,
                    UnitId = 1

                };
                ProductEntity product2 = new ProductEntity
                {
                    Code = "PR2",
                    Description = "Prod2",
                    IsAvailable = true,
                    Price = 13,
                    DeliveryDate = DateTime.Now,
                    Name = "PRD2",
                    TypeId = 2,
                    UnitId = 2

                };
                ProductEntity product3 = new ProductEntity
                {
                    Code = "PR3",
                    Description = "Prod3",
                    IsAvailable = false,
                    Price = 15,
                    DeliveryDate = DateTime.Now,
                    Name = "PRD3",
                    TypeId = 1,
                    UnitId = 1
                };
                ProductEntity product4 = new ProductEntity
                {
                    Code = "PR4",
                    Description = "Prod4",
                    IsAvailable = true,
                    Price = 13,
                    DeliveryDate = DateTime.Now,
                    Name = "PRD4",
                    TypeId = 2,
                    UnitId = 2
                };

                context.Products.Add(product1);
                context.Products.Add(product2);
                context.Products.Add(product3);
                context.Products.Add(product4);
                context.SaveChanges();
            }
        }
    }
}
