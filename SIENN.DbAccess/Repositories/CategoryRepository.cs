﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace SIENN.DbAccess.Repositories
{
    public class CategoryRepository : GenericRepository<CategoryEntity>
    {
        public CategoryRepository(SIENNContext dbContext)
            : base(dbContext)
        {}
    }
}
