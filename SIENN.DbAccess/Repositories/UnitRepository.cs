﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SIENN.DbAccess.Repositories
{
    public class UnitRepository : GenericRepository<UnitEntity>
    {
        public UnitRepository(SIENNContext dbContext)
            : base(dbContext)
        { }
    }
}
