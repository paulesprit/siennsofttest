﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SIENN.DbAccess.Repositories
{
    public class TypeRepository : GenericRepository<TypeEntity>
    {
        public TypeRepository(SIENNContext dbContext)
            : base(dbContext)
        { }
    }
}
