﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace SIENN.DbAccess.Repositories
{
    public class ProductRepository : GenericRepository<ProductEntity>
    {
        public ProductRepository(SIENNContext dbContext)
            : base(dbContext)
        { }

        public override IEnumerable<ProductEntity> Find(Expression<Func<ProductEntity, bool>> predicate)
        {
            return _entities.Include(p => p.Categories).Where(predicate);
        }

        public override ProductEntity Get(int id)
        {
            return _entities.Include(p => p.Categories)
                .Include(p => p.Type)
                .Include(p => p.Unit)
                .SingleOrDefault(p => p.Id == id);
        }
    }
}
