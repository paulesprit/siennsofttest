  
  --1.	Unavailable products which delivery is expected in current month
  SELECT *  FROM [dbo].[Products] WHERE IsAvailable = 0 and MONTH(DeliveryDate) = MONTH(GetDate())
  
  --2.	Available products that are assigned to more than one category
  SELECT * From [dbo].[Products]  WHERE IsAvailable = 1 and id in 
	(SELECT ProductId FROM [dbo].[ProductCategoryEntity] 
	GROUP BY ProductId HAVING COUNT(CategoryId)>1)
  
  --3.	Top 3 categories with info about numbers of available, assigned products with its mean price in category (top 3 should display categories which mean price is the highest)
    SELECT TOP 3 
	[Id],
    [Code],
    [Description],
    [NumbersAvailableProducts],
    [AvgPrice]
  FROM [dbo].[Categories]
  INNER JOIN 
  (SELECT [CategoryId], Count(ProductId) AS NumbersAvailableProducts, AVG([Price]) AvgPrice
  FROM [dbo].[ProductCategoryEntity]
  INNER JOIN [dbo].[Products] AS Products
  ON [ProductId]=Products.Id and IsAvailable = 1 
  GROUP BY [CategoryId]) ProductStats ON Id = ProductStats.CategoryId
  ORDER BY AvgPrice DESC