﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SIENN.Interfaces.Category
{
    public interface ICategoryService
    {
        IList<CategoryDto> List();
        CategoryDto Add(CategoryDto categoryDto);
        CategoryDto Update(CategoryDto categoryDto);
        void Remove(int categoryId);
        CategoryDto Get(int categoryId);
    }
}
