﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SIENN.Interfaces.Category
{
    public class CategoryDto
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
    }
}
