﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SIENN.Interfaces
{
    public interface IUnitOfWork
    {
        void Save();
    }
}
