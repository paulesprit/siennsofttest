﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SIENN.Interfaces.Type
{
    public interface ITypeService
    {
        IList<TypeDto> List();
        TypeDto Add(TypeDto typeDto);
        TypeDto Update(TypeDto typeDto);
        void Remove(int typeId);
        TypeDto Get(int typeId);
    }
}
