﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SIENN.Interfaces.Type
{
    public class TypeDto
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
    }
}
