﻿using SIENN.Interfaces.Category;
using System;
using System.Collections.Generic;
using System.Text;

namespace SIENN.Interfaces.Product
{
    public class ProductInfoDto
    {
        public string ProductDescription { get; set; }
        public string Price { get; set; }
        public string IsAvailable { get; set; }
        public string DeliveryDate { get; set; }
        public int CategoriesCount { get; set; }
        public string Type { get; set; }
        public string Unit { get; set; }        
    }
}
