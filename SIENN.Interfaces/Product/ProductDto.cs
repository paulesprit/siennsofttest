﻿using SIENN.Interfaces.Category;
using System;
using System.Collections.Generic;
using System.Text;

namespace SIENN.Interfaces.Product
{
    public class ProductDto
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public decimal Price { get; set; }
        public bool IsAvailable { get; set; }
        public DateTime? DeliveryDate { get; set; }
        public int TypeId { get; set; }
        public int UnitId { get; set; }
        public List<ProductCategoryDto> Categories { get; set; }
        public string Name { get; set; }
    }
}
