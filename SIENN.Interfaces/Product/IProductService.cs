﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SIENN.Interfaces.Product
{
    public interface IProductService
    {
        Task<List<ProductDto>> ListAsync();
        Task<List<ProductDto>> ListFilterAsync(int unitId, int typeId, int categoryId);
        Task<List<ProductDto>> ListAvailableAsync(int start, int count);
        Task<ProductDto> AddAsync(ProductDto productDto);
        Task<ProductDto> UpdateAsync(ProductDto productDto);
        Task RemoveAsync(int productId);
        Task<ProductDto> GetAsync(int productId);
        Task<ProductInfoDto> GetProductInfoAsync(int productId);

    }
}
