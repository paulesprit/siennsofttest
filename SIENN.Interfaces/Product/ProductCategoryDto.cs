﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SIENN.Interfaces.Product
{
    public class ProductCategoryDto
    {
        public int ProductId { get; set; }
        public int CategoryId { get; set; }
    }
}
