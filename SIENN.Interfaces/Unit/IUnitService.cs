﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SIENN.Interfaces.Unit
{
    public interface IUnitService
    {
        IList<UnitDto> List();
        UnitDto Add(UnitDto unitDto);
        UnitDto Update(UnitDto unitDto);
        void Remove(int unitId);
        UnitDto Get(int unitId);
    }
}
