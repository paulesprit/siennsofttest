﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SIENN.Interfaces.Unit
{
    public class UnitDto
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
    }
}
