using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SIENN.DbAccess;
using SIENN.DbAccess.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using SIENN.Interfaces.Product;

namespace SIENN.Services.Tests
{
    [TestClass]
    public class ProductServiceTests
    {
        

        [TestMethod]
        public void List()
        {
            using (var context = TestData.GetInMemoryContext())
            {
                ProductRepository repository = new ProductRepository(context);
                UnitOfWork unitOfWork = new UnitOfWork(context);
                ProductService service = new ProductService(repository, unitOfWork);

                var products = service.ListAsync();
                Assert.IsNotNull(products);
                Assert.IsTrue(products.Result.Count == 3);
            }

        }

        [TestMethod]
        public void ListAvailable()
        {
            using (var context = TestData.GetInMemoryContext())
            {
                ProductRepository repository = new ProductRepository(context);
                UnitOfWork unitOfWork = new UnitOfWork(context);
                ProductService service = new ProductService(repository, unitOfWork);

                var products = service.ListAvailableAsync(0, 3);
                Assert.IsNotNull(products);
                Assert.AreEqual(3, products.Result.Count);
            } 
        } 

        [TestMethod]
        public void ListFilter()
        {
            using (var context = TestData.GetInMemoryContext())
            {
                ProductRepository repository = new ProductRepository(context);
                UnitOfWork unitOfWork = new UnitOfWork(context);
                ProductService service = new ProductService(repository, unitOfWork);

                var products = service.ListFilterAsync(TestData.OneUnit.Id, TestData.FoodType.Id, TestData.BeerCategory.Id);
                Assert.IsNotNull(products);
                Assert.AreEqual(1, products.Result.Count);
            }
        }

        [TestMethod]
        public void Get()
        {
            using (var context = TestData.GetInMemoryContext())
            {
                ProductRepository repository = new ProductRepository(context);
                UnitOfWork unitOfWork = new UnitOfWork(context);
                ProductService service = new ProductService(repository, unitOfWork);

                var product = service.GetAsync(TestData.Product1.Id);
                Assert.IsNotNull(product);
                Assert.AreEqual(TestData.Product1.Id, product.Id);                
            }
        }

        [TestMethod]
        public void Add()
        {
            using (var context = TestData.GetInMemoryContext())
            {
                ProductRepository repository = new ProductRepository(context);
                UnitOfWork unitOfWork = new UnitOfWork(context);
                ProductService service = new ProductService(repository, unitOfWork);

                var product = new ProductDto()
                {
                    Code = "TestCode",
                    DeliveryDate = new DateTime(2017, 1, 1),
                    Categories = new List<ProductCategoryDto>(){new ProductCategoryDto(){CategoryId = TestData.AlcoholCategory.Id}}
                };

                var added = service.AddAsync(product);
                Assert.IsNotNull(added);
                Assert.AreNotEqual(0, product.Id);
                Assert.AreEqual(TestData.AlcoholCategory.Id, product.Categories.Single().CategoryId);
            }
        }

        [TestMethod]
        public void Update()
        {
            using (var context = TestData.GetInMemoryContext())
            {
                ProductRepository repository = new ProductRepository(context);
                UnitOfWork unitOfWork = new UnitOfWork(context);
                ProductService service = new ProductService(repository, unitOfWork);

                var product = TestData.Product1;
                Assert.IsNotNull(product);
                Assert.AreNotEqual(0, product.Id);
                context.Entry(product).State = EntityState.Detached;

                var productDto = new ProductDto()
                {
                    Id = product.Id,
                    Code = Guid.NewGuid().ToString(),
                    Categories = new List<ProductCategoryDto>()
                    {
                        new ProductCategoryDto(){CategoryId = TestData.AlcoholCategory.Id},
                        new ProductCategoryDto() { CategoryId = TestData.BeerCategory.Id },
                        new ProductCategoryDto() { CategoryId = TestData.WineCategory.Id }
                    }
                };

                var added = service.UpdateAsync(productDto);
                Assert.IsNotNull(added);
                Assert.AreEqual(productDto.Id, added.Id);
                Assert.AreEqual(productDto.Code, added.Result.Code);
                Assert.AreEqual(3, added.Result.Categories.Count);
            }
        }

        [TestMethod]
        public void Remove()
        {
            using (var context = TestData.GetInMemoryContext())
            {
                ProductRepository repository = new ProductRepository(context);
                UnitOfWork unitOfWork = new UnitOfWork(context);
                ProductService service = new ProductService(repository, unitOfWork);


                var toDelete = context.Products.First();
                Assert.IsNotNull(toDelete);
                context.Entry(toDelete).State = EntityState.Detached;

                service.RemoveAsync(toDelete.Id);
                context.SaveChanges();
                Assert.IsNull(repository.Get(toDelete.Id));

            }
        }

        [TestMethod]
        public void GetProductInfo()
        {
            using (var context = TestData.GetInMemoryContext())
            {
                ProductRepository repository = new ProductRepository(context);
                UnitOfWork unitOfWork = new UnitOfWork(context);
                ProductService service = new ProductService(repository, unitOfWork);

                var product = service.GetProductInfoAsync(TestData.Product1.Id);
                Assert.IsNotNull(product);
                Assert.AreNotEqual(product.Result.ProductDescription, string.Empty);
                Assert.IsTrue(product.Result.Price.Contains("zl"));
            }
        }
    }
}
