using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SIENN.DbAccess;
using SIENN.DbAccess.Repositories;
using SIENN.Interfaces.Unit;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SIENN.Services.Tests
{
    [TestClass]
    public class UnitServiceTests
    {
        

        [TestMethod]
        public void List()
        {
            using (var context = TestData.GetInMemoryContext())
            {
                UnitRepository repository = new UnitRepository(context);
                UnitOfWork unitofWork = new UnitOfWork(context);
                UnitService service = new UnitService(repository, unitofWork);

                var units = service.List();
                Assert.IsNotNull(units);
                Assert.AreEqual(2, units.Count);
            }
        }

        [TestMethod]
        public void Add()
        {
            using (var context = TestData.GetInMemoryContext())
            {
                UnitRepository repository = new UnitRepository(context);
                UnitOfWork unitofWork = new UnitOfWork(context);
                UnitService service = new UnitService(repository, unitofWork);

                UnitDto dto = new UnitDto
                {
                    Code = Guid.NewGuid().ToString(),
                    Description = Guid.NewGuid().ToString()
                };

                service.Add(dto);
                context.SaveChanges();

                Assert.AreNotEqual(0, dto.Id);
                var added = repository.Get(dto.Id);
                Assert.IsNotNull(added);
                Assert.AreEqual(dto.Code, added.Code);
                Assert.AreEqual(dto.Description, added.Description);
            }
        }

        [TestMethod]
        public void Update()
        {
            using (var context = TestData.GetInMemoryContext())
            {
                UnitRepository repository = new UnitRepository(context);
                UnitOfWork unitofWork = new UnitOfWork(context);
                UnitService service = new UnitService(repository, unitofWork);

                var toUpdate = context.Units.First();
                Assert.IsNotNull(toUpdate);
                Assert.AreNotEqual(0, toUpdate.Id);
                context.Entry(toUpdate).State = EntityState.Detached;

                UnitDto dto = new UnitDto
                {
                    Id = toUpdate.Id,
                    Code = Guid.NewGuid().ToString(),
                    Description = Guid.NewGuid().ToString()
                };

                service.Update(dto);
                context.SaveChanges();

                var unit = repository.Get(toUpdate.Id);
                Assert.AreEqual(unit.Code, dto.Code);
                Assert.AreEqual(unit.Description, dto.Description);
            }
        }

        [TestMethod]
        public void Remove()
        {
            using (var context = TestData.GetInMemoryContext())
            {
                UnitRepository repository = new UnitRepository(context);
                UnitOfWork unitofWork = new UnitOfWork(context);
                UnitService service = new UnitService(repository, unitofWork);

                var toDelete = context.Units.First();
                Assert.IsNotNull(toDelete);
                context.Entry(toDelete).State = EntityState.Detached;

                service.Remove(toDelete.Id);
                context.SaveChanges();
                Assert.IsNull(repository.Get(toDelete.Id));
            }
        }

        [TestMethod]
        public void Get()
        {
            using (var context = TestData.GetInMemoryContext())
            {
                UnitRepository repository = new UnitRepository(context);
                UnitOfWork unitofWork = new UnitOfWork(context);
                UnitService service = new UnitService(repository, unitofWork);

                var unit = service.Get(TestData.OneUnit.Id);
                Assert.IsNotNull(unit);
                Assert.AreEqual(TestData.OneUnit.Id, unit.Id);
            }
        }
    }
}
