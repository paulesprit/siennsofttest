using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SIENN.DbAccess;
using SIENN.DbAccess.Repositories;
using SIENN.Interfaces.Type;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SIENN.Services.Tests
{
    [TestClass]
    public class TypeServiceTests
    {
        

        [TestMethod]
        public void List()
        {
            using (var context = TestData.GetInMemoryContext())
            {
                TypeRepository repository = new TypeRepository(context);
                UnitOfWork unitofWork = new UnitOfWork(context);
                TypeService service = new TypeService(repository, unitofWork);

                var types = service.List();
                Assert.IsNotNull(types);
                Assert.AreEqual(2, types.Count);
            }

        }

        [TestMethod]
        public void Add()
        {
            using (var context = TestData.GetInMemoryContext())
            {
                TypeRepository repository = new TypeRepository(context);
                UnitOfWork unitofWork = new UnitOfWork(context);
                TypeService service = new TypeService(repository, unitofWork);

                TypeDto dto = new TypeDto
                {
                    Code = Guid.NewGuid().ToString(),
                    Description = Guid.NewGuid().ToString()
                };

                service.Add(dto);
                context.SaveChanges();

                Assert.AreNotEqual(0, dto.Id);
                var added = repository.Get(dto.Id);
                Assert.IsNotNull(added);
                Assert.AreEqual(dto.Code, added.Code);
                Assert.AreEqual(dto.Description, added.Description);
            }
        }

        [TestMethod]
        public void Update()
        {
            using (var context = TestData.GetInMemoryContext())
            {
                TypeRepository repository = new TypeRepository(context);
                UnitOfWork unitofWork = new UnitOfWork(context);
                TypeService service = new TypeService(repository, unitofWork);

                var toUpdate = context.Types.First();
                Assert.IsNotNull(toUpdate);
                Assert.AreNotEqual(0, toUpdate.Id);
                context.Entry(toUpdate).State = EntityState.Detached;

                TypeDto dto = new TypeDto
                {
                    Id = toUpdate.Id,
                    Code = Guid.NewGuid().ToString(),
                    Description = Guid.NewGuid().ToString()
                };

                service.Update(dto);
                context.SaveChanges();

                var type = repository.Get(toUpdate.Id);
                Assert.AreEqual(type.Code, dto.Code);
                Assert.AreEqual(type.Description, dto.Description);
            }
        }

        [TestMethod]
        public void Remove()
        {
            using (var context = TestData.GetInMemoryContext())
            {
                TypeRepository repository = new TypeRepository(context);
                UnitOfWork unitofWork = new UnitOfWork(context);
                TypeService service = new TypeService(repository, unitofWork);

                var toDelete = context.Types.First();
                Assert.IsNotNull(toDelete);
                context.Entry(toDelete).State = EntityState.Detached;

                service.Remove(toDelete.Id);
                context.SaveChanges();
                Assert.IsNull(repository.Get(toDelete.Id));
            }
        }

        [TestMethod]
        public void Get()
        {
            using (var context = TestData.GetInMemoryContext())
            {
                TypeRepository repository = new TypeRepository(context);
                UnitOfWork unitofWork = new UnitOfWork(context);
                TypeService service = new TypeService(repository, unitofWork);

                var type = service.Get(TestData.FoodType.Id);
                Assert.IsNotNull(type);
                Assert.AreEqual(TestData.FoodType.Id, type.Id);
            }
        }
    }
}
