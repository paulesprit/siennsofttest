using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SIENN.DbAccess;
using SIENN.DbAccess.Repositories;
using SIENN.Interfaces.Category;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SIENN.Services.Tests
{
    [TestClass]
    public class CategoryServiceTests
    {  
        [TestMethod]
        public void List()
        {
            using (var context = TestData.GetInMemoryContext()) {
                CategoryRepository repository = new CategoryRepository(context);
                UnitOfWork unitOfWork = new UnitOfWork(context);
                CategoryService service = new CategoryService(repository, unitOfWork);

                var categories = service.List();
                Assert.IsNotNull(categories);
                Assert.IsTrue(categories.Count == 3);
                Assert.IsNotNull(categories.SingleOrDefault(c=>c.Code == "BR"));
                Assert.IsNotNull(categories.SingleOrDefault(c=>c.Code == "WN"));
                Assert.IsNotNull(categories.SingleOrDefault(c=>c.Code == "ALC"));
            }
        }

        [TestMethod]
        public void Add()
        {
            using (var context = TestData.GetInMemoryContext())
            {
                CategoryRepository repository = new CategoryRepository(context);
                UnitOfWork unitOfWork = new UnitOfWork(context);
                CategoryService service = new CategoryService(repository, unitOfWork);

                CategoryDto dto = new CategoryDto {
                    Code = Guid.NewGuid().ToString(),
                    Description = Guid.NewGuid().ToString()
                };

                service.Add(dto);
                context.SaveChanges();

                Assert.AreNotEqual(0, dto.Id);
                var added = repository.Get(dto.Id);
                Assert.IsNotNull(added);
                Assert.AreEqual(dto.Code, added.Code);
                Assert.AreEqual(dto.Description, added.Description);
            }
        }

        [TestMethod]
        public void Update()
        {
            using (var context = TestData.GetInMemoryContext())
            {
                CategoryRepository repository = new CategoryRepository(context);
                UnitOfWork unitOfWork = new UnitOfWork(context);
                CategoryService service = new CategoryService(repository, unitOfWork);

                var toUpdate = context.Categories.First();
                Assert.IsNotNull(toUpdate);
                Assert.AreNotEqual(0, toUpdate.Id);
                context.Entry(toUpdate).State = EntityState.Detached;

                CategoryDto dto = new CategoryDto
                {
                    Id = toUpdate.Id,
                    Code = Guid.NewGuid().ToString(),
                    Description = Guid.NewGuid().ToString()
                };

                service.Update(dto);
                context.SaveChanges();

                var category = repository.Get(toUpdate.Id);
                Assert.AreEqual(category.Code, dto.Code);
                Assert.AreEqual(category.Description, dto.Description);
            }
        }

        [TestMethod]
        public void Remove()
        {
            using (var context = TestData.GetInMemoryContext())
            {
                CategoryRepository repository = new CategoryRepository(context);
                UnitOfWork unitOfWork = new UnitOfWork(context);
                CategoryService service = new CategoryService(repository, unitOfWork);

                var toDelete = context.Categories.First(); 
                Assert.IsNotNull(toDelete);
                context.Entry(toDelete).State = EntityState.Detached;

                service.Remove(toDelete.Id);
                context.SaveChanges();
                Assert.IsNull(repository.Get(toDelete.Id));
            }

        }

        [TestMethod]
        public void Get()
        {
            using (var context = TestData.GetInMemoryContext())
            {
                CategoryRepository repository = new CategoryRepository(context);
                UnitOfWork unitOfWork = new UnitOfWork(context);
                CategoryService service = new CategoryService(repository, unitOfWork);

                var category = service.Get(TestData.BeerCategory.Id);
                Assert.IsNotNull(category);
                Assert.AreEqual(TestData.BeerCategory.Id, category.Id);
            }
        }
    }
}
