﻿using Microsoft.EntityFrameworkCore;
using SIENN.DbAccess;
using System;
using System.Collections.Generic;

namespace SIENN.Services.Tests
{
    public class TestData
    {
        public static CategoryEntity BeerCategory;
        public static CategoryEntity WineCategory;
        public static CategoryEntity AlcoholCategory;

        public static TypeEntity FoodType;
        public static TypeEntity DrinkType;

        public static UnitEntity OneUnit;
        public static UnitEntity SecondUnit;

        public static ProductEntity Product1;
        public static ProductEntity Product2;
        public static ProductEntity Product3;

        public static SIENNContext GetInMemoryContext()
        {
            var databaseName = Guid.NewGuid().ToString();
            var options = new DbContextOptionsBuilder<SIENNContext>()
                              .UseInMemoryDatabase(databaseName)
                              .Options;
            using (var context = new SIENNContext(options))
            {
                BeerCategory = new CategoryEntity { Code = "BR", Description = "Beers" };
                WineCategory = new CategoryEntity { Code = "WN", Description = "Wines" };
                AlcoholCategory = new CategoryEntity { Code = "ALC", Description = "Alcohol" };

                FoodType = new TypeEntity { Code = "111", Description = "Food" };
                DrinkType = new TypeEntity { Code = "222", Description = "Water" };

                OneUnit = new UnitEntity { Code = "333", Description = "One" };
                SecondUnit = new UnitEntity { Code = "555", Description = "Two" };


                context.Categories.Add(BeerCategory);
                context.Categories.Add(WineCategory);
                context.Categories.Add(AlcoholCategory);
   
                context.Types.Add(FoodType);
                context.Types.Add(DrinkType);

                context.Units.Add(OneUnit);
                context.Units.Add(SecondUnit);

                context.SaveChanges();

                Product1 = new ProductEntity { Name = "La Trappe Isid'or", Code = "11", Description = "La Trappe Isid'or", Price = 10, IsAvailable = true, DeliveryDate = new DateTime(2017, 12, 25), TypeId = FoodType.Id, UnitId = SecondUnit.Id };
                context.Products.Add(Product1);
                Product2 = new ProductEntity { Name = "Bush", Code = "12", Description = "La Trappe Isid'or", Price = 10, IsAvailable = true, DeliveryDate = new DateTime(2017, 12, 26), TypeId = FoodType.Id, UnitId = OneUnit.Id };
                context.Products.Add(Product2);
                Product3 = new ProductEntity { Name = "La Trappe Isid'or", Code = "13", Description = "La Trappe Isid'or", Price = 10, IsAvailable = true, DeliveryDate = new DateTime(2017, 12, 24), TypeId = DrinkType.Id, UnitId = SecondUnit.Id };
                context.Products.Add(Product3);
                context.SaveChanges();

                Product1.Categories = new List<ProductCategoryEntity>();
                var productCategoryEntity3 = new ProductCategoryEntity
                {
                    Product = Product1,
                    Category = WineCategory
                };
                Product1.Categories.Add(productCategoryEntity3);
                var productCategoryEntity2 = new ProductCategoryEntity
                {
                    Product = Product1,
                    Category = AlcoholCategory
                };
                Product1.Categories.Add(productCategoryEntity2);

                Product2.Categories = new List<ProductCategoryEntity>();
                var productCategoryEntity1 = new ProductCategoryEntity { Product = Product2, Category = BeerCategory };
                Product2.Categories.Add(productCategoryEntity1);
                context.SaveChanges();
            }
            return new SIENNContext(options);
        }

        public static void DetachEntity(DbContext context, Object entity)
        {
            context.Entry(entity).State = EntityState.Detached;
        }
    }
}
